import * as React from "react";
import * as ReactDOM from "react-dom";
import { App } from "./App";


const el = document.querySelector("#app");
ReactDOM.render(<App />, el);
