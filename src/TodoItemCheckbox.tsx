import * as React from "react";

type Props = {
    checked: boolean;
}

export function TodoItemCheckbox(props: Props) {

    return (
        <input type="checkbox" checked={props.checked} />
    )

}