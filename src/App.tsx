import * as  React from "react";
import { TodoItem } from "./TodoItem";
import { TodoInput } from "./TodoInput";

interface Todo {
    text: string;
    checked: boolean;
}

interface State {
    todos: Todo[];
}

export class App extends React.Component<{}, State> {
    state: State = {
        todos: [
            { checked: false, text: "todo1" },
            { checked: false, text: "todo2" },
            { checked: true, text: "todo3" },
            { checked: false, text: "todo4" },
        ]
    };

    render() {
        return <div>
            {this.state.todos.map((todo, i) => (
                <TodoItem checked={todo.checked} text={todo.text} onClick={() => {
                    const { todos } = this.state;
                    todos[i].checked = !todos[i].checked;
                    this.setState({ todos })
                }} />
            ))}

            <TodoInput onSubmit={text => {
                const { todos } = this.state;
                todos.push({ checked: false, text });
                this.setState({ todos });
            }} />
        </div>
    }
}

