import * as React from "react";
import { Input, Button } from "@material-ui/core";


type Props = {
    onSubmit: (text: string) => void;
}

type State = {
    todoText: string;
}

export class TodoInput extends React.Component<Props, State> {
    state: State = {
        todoText: "(empty)"
    };

    handleSubmit = () => {
        this.props.onSubmit(this.state.todoText);
        this.setState({ todoText: "" });
    };

    render() {
        return (
            <div style={{ display: "flex" }}>
                <Input
                    onChange={ev => {
                        this.setState({ todoText: ev.target.value });
                    }}
                    value={this.state.todoText}
                />
                <Button
                    variant={"contained"}
                    onClick={() => this.handleSubmit()}
                >Add</Button>
            </div>
        )
    }
}