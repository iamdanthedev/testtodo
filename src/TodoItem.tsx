import * as React from "react";
import { TodoItemCheckbox } from "./TodoItemCheckbox";

type Props = {
    checked: boolean;
    text: string;
    onClick: () => void;
}

export class TodoItem extends React.Component<Props> {

    render() {
        return (
            <div onClick={this.props.onClick}>
                <TodoItemCheckbox checked={this.props.checked} />
                <span>{this.props.text}</span>
            </div>
        )
    }
}

