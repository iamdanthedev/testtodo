const path = require('path');

module.exports = {
    mode: "development",
    entry: './src/index.tsx',

    devtool: "source-map",

    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: [".ts", ".tsx", ".js"]
    },

    module: {
        rules: [
            // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
            { test: /\.tsx?$/, loader: "ts-loader" }
        ]
    },

    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'static')
    }
};